
export KAGGLE_CONFIG_DIR=/root/kaggleremotejob
cp kernelgpu/kernel-metadata.json.generatetext.remotejob runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglesipvip
cp kernelgpu/kernel-metadata.json.generatetext.sipvip runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglealmazseo
cp kernelgpu/kernel-metadata.json.generatetext.almazseo runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kaggleipotecafi
cp kernelgpu/kernel-metadata.json.generatetext.ipotecafi runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglekagkagdevprod
cp kernelgpu/kernel-metadata.json.generatetext.kagkagdevprod runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglesupportsupport
cp kernelgpu/kernel-metadata.json.generatetext.supportsupport runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglealmazurov
cp kernelgpu/kernel-metadata.json.generatetext.almazurov runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglealfredoformosa
cp kernelgpu/kernel-metadata.json.generatetext.alfredoformosa runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglereinhardhaber
cp kernelgpu/kernel-metadata.json.generatetext.reinhardhaber runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kaggleremotejobtest
cp kernelgpu/kernel-metadata.json.generatetext.remotejobtest runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kaggledevelopmentsupport
cp kernelgpu/kernel-metadata.json.generatetext.developmentsupport runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglegregsip
cp kernelgpu/kernel-metadata.json.generatetext.gregsip runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kaggleelisasipvip
cp kernelgpu/kernel-metadata.json.generatetext.elisasipvip runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglegregsecond
cp kernelgpu/kernel-metadata.json.generatetext.gregsecond runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglemadisfirst
cp kernelgpu/kernel-metadata.json.generatetext.madisfirst runprod/kernel-metadata.json
kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglemaksmaksmaz
cp kernelgpu/kernel-metadata.json.generatetext.maksmaksmaz runprod/kernel-metadata.json
/root/anaconda3/envs/gpt2finnishrun/bin/kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglemarkovkolj
cp kernelgpu/kernel-metadata.json.generatetext.markovkolj runprod/kernel-metadata.json
/root/anaconda3/envs/gpt2finnishrun/bin/kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglejuhalinus
cp kernelgpu/kernel-metadata.json.generatetext.juhalinus runprod/kernel-metadata.json
/root/anaconda3/envs/gpt2finnishrun/bin/kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglelinuskari
cp kernelgpu/kernel-metadata.json.generatetext.linuskari runprod/kernel-metadata.json
/root/anaconda3/envs/gpt2finnishrun/bin/kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglekariostman
cp kernelgpu/kernel-metadata.json.generatetext.kariostman runprod/kernel-metadata.json
/root/anaconda3/envs/gpt2finnishrun/bin/kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kaggleostmanrita
cp kernelgpu/kernel-metadata.json.generatetext.ostmanrita runprod/kernel-metadata.json
/root/anaconda3/envs/gpt2finnishrun/bin/kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kaggleritakuuma
cp kernelgpu/kernel-metadata.json.generatetext.ritakuuma runprod/kernel-metadata.json
/root/anaconda3/envs/gpt2finnishrun/bin/kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglekuumaselko
cp kernelgpu/kernel-metadata.json.generatetext.kuumaselko runprod/kernel-metadata.json
/root/anaconda3/envs/gpt2finnishrun/bin/kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kaggleselkobert
cp kernelgpu/kernel-metadata.json.generatetext.selkobert runprod/kernel-metadata.json
/root/anaconda3/envs/gpt2finnishrun/bin/kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kagglebertturva
cp kernelgpu/kernel-metadata.json.generatetext.bertturva runprod/kernel-metadata.json
/root/anaconda3/envs/gpt2finnishrun/bin/kaggle kernels push -p runprod/

export KAGGLE_CONFIG_DIR=/root/kaggleturvasairas
cp kernelgpu/kernel-metadata.json.generatetext.turvasairas runprod/kernel-metadata.json
/root/anaconda3/envs/gpt2finnishrun/bin/kaggle kernels push -p runprod/


unset KAGGLE_CONFIG_DIR
cp kernelgpu/kernel-metadata.json.generatetext.alesandermazurov runprod/kernel-metadata.json
kaggle kernels push -p runprod/




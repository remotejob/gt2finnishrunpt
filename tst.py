import os

kaggleidenv = os.getenv('KAGGLE_CONFIG_DIR')
kaggleidspl = str(kaggleidenv).split('/')
kaggleid = kaggleidspl[2].replace('kaggle','')

dataset = '../input/'+kaggleid+'data'

print(dataset)
